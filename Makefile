build_tag ?= papyri-xsugar

.PHONY: build
build:
	docker build -t $(build_tag) https://github.com/papyri/xsugar.git
